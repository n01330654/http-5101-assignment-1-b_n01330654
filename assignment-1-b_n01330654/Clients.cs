﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignment_1_b_n01330654
{
    public class Clients
    {
        private string clientLname; 
        private string clientFname;
        private string clientPhone;
        private string clientEmail;

        public Clients()
        {

        }

        public string ClientLname
        {
            get { return clientLname; }
            set { clientLname = value; }
        }
        public string ClientFname
        {
            get { return clientFname; }
            set { clientFname = value; }
        }
        public string ClientPhone
        {
            get { return clientPhone; }
            set { clientPhone = value; }
        }
        public string ClientEmail
        {
            get { return clientEmail; }
            set { clientEmail = value; }
        }

    }
}