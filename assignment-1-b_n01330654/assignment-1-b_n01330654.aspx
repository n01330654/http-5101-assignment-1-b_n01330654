﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="assignment-1-b_n01330654.aspx.cs" Inherits="assignment_1_b_n01330654.assignment_1_b_n01330654" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <br />
            <p><strong>HOTEL ROOM RESERVATION</strong></p>
            <%-- START CLIENT INFORMATIONS  --%>
            <asp:Label runat="server" ID="labelFname" text="First Name:"></asp:Label>
            <br />
            <asp:TextBox runat="server" ID="clientFname" placeholder="Enter your First Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your First Name" ControlToValidate="clientFname" ID="validatorClientFname"></asp:RequiredFieldValidator>
            <br />
            <br />
            <asp:Label runat="server" ID="labelLname" text="Last Name:   "></asp:Label>
            <br />
            <asp:TextBox runat="server" ID="clientLname" placeholder="Enter your Last Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your LastName" ControlToValidate="clientLname" ID="validatorClientLname"></asp:RequiredFieldValidator>
            <br />
            <br />
            <asp:Label runat="server" ID="labelEmail" Text="Email:"></asp:Label><br />
            <asp:TextBox runat="server" ID="clientEmail" placeholder="xxxxx@email.com"></asp:TextBox>
            <asp:RequiredFieldValidator ID="requiredEmail" runat="server" ErrorMessage="Please enter your Email" ControlToValidate="clientEmail"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="validatorClientEmail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
            <br />
            <br />
            <asp:Label runat="server" Text="Phone Number:"></asp:Label>
            <br />
            <asp:TextBox runat="server" ID="clientPhone" placeholder="Enter your Phone Number"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a Phone Number" ControlToValidate="clientPhone" ID="validatorClientPhone"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID= "compareClientPhone" runat="server" ControlToValidate="clientPhone" Type="String" Operator="NotEqual" ValueToCompare="4168013333" ErrorMessage="Invalid phone number"></asp:CompareValidator>
            <%-- END CLIENT INFORMATIONS  --%>
            <br />
            <br />
            <%-- START OF BOOKING INFORMATIONS  --%>
            <asp:Label runat="server" Text="Check-In Date"></asp:Label>
            <br />
            <asp:TextBox runat="server" ID="bookingCheckin" placeholder="DD/MM/YYYY"></asp:TextBox>
            <asp:RequiredFieldValidator ID="validatorBookingCheckin" runat="server" ErrorMessage="Please enter Check-in Date" ControlToValidate="bookingCheckin"></asp:RequiredFieldValidator>
            <asp:regularexpressionvalidator id="validformatdate" runat="server" errormessage="invalid date format" controltovalidate="bookingCheckin"  validationexpression="(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/\d{4}"></asp:regularexpressionvalidator>
            <br />
            <br />
            <asp:Label runat="server" Text="Number of Nights"></asp:Label>
            <br />
            <asp:TextBox ID="bookingNumNights" runat="server" placeholder="Enter number of nights"></asp:TextBox>
            <asp:RequiredFieldValidator ID="validbookingNumNights" runat="server" ErrorMessage="Please enter Number of Nights" ControlToValidate="bookingNumNights"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="validatorBookingNumNights" runat="server" ControlToValidate="bookingNumNights" Type="Integer" MinimumValue="1" MaximumValue="100" ErrorMessage="Enter a valid number of Nights (between 1 and 100)"></asp:RangeValidator>
            <%-- END OF BOOKING INFORMATIONS  --%>
            <br />
            <br />
            <%-- START OF HOTEL INFORMATION OPTIONS  --%>
            <asp:Label runat="server" Text="Number of Rooms:"></asp:Label>
            <br />
            <asp:TextBox runat="server" ID="hotelNumRoom" placeholder="Number of Rooms"></asp:TextBox>
            <asp:RequiredFieldValidator ID="validHotelNumRoom" runat="server" ErrorMessage="Please enter Number of Rooms" ControlToValidate="hotelNumRoom"></asp:RequiredFieldValidator>
            <asp:RangeValidator runat="server" ControlToValidate="hotelNumRoom" Type="Integer" MinimumValue="1" MaximumValue="10" ErrorMessage="Enter a valid number of Rooms (between 1 and 10)"></asp:RangeValidator>
            <br />
            <br />
            <asp:Label runat="server" Text="Room Type"></asp:Label>
            <br />
            <asp:DropDownList runat="server" ID="hotelRoomType" >
                <asp:ListItem Value="T" Text="Twin Room"></asp:ListItem>
                <asp:ListItem Value="D" Text="Deluxe Room"></asp:ListItem>
                <asp:ListItem Value="F" Text="Family Room"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
            <asp:Label runat="server" Text="Please Choose"></asp:Label>
            <br />
            <asp:RadioButtonList runat="server" ID="hotelSmokingRoom">
                <asp:ListItem Value="SM" Text="Smoking Room"  Selected ="True">Smoking Room</asp:ListItem>
                <asp:ListItem Value="NSM" Text="Non-Smoking Room" >Non-Smoking Room</asp:ListItem>
            </asp:RadioButtonList>
            <br />
            <asp:Label runat="server" ID="addService" Text="Additional Services" ></asp:Label>
            <br />
             <div id="hotelAddServices" runat="server" >
            <asp:CheckBox runat="server" ID="hotelService1" Text="Airport Pick-up/Drop-off" /><br />
            <asp:CheckBox runat="server" ID="hotelService2" Text="Early Check-in" /><br />
            <asp:CheckBox runat="server" ID="hotelService3" Text="Late Checkout(Additional fees may apply)" /><br />
            </div>
            <%-- END OF HOTEL INFORMATION OPTIONS  --%>
            <br />
            <asp:ValidationSummary ID="ValidationSummaryHotelBooking" runat="server" ForeColor="red"/> 
            <br />
            <asp:Button runat="server" ID="hotelBook" OnClick="HotelBook" text="Submit"/>
            <br /><br />

            <div runat="server" ID="BookOrderRes"></div>
        </div>
    </form>
</body>
</html>
