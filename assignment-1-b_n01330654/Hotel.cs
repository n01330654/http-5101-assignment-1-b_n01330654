﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace assignment_1_b_n01330654
{
    public class Hotel
    {

        public string roomType;
        public int numRoom;
        public string smokingRoom;
        public List<string> services;

        public Hotel(string rt, int nr,  string sm, List<string> se)
        {

            roomType = rt;
            numRoom = nr;
            smokingRoom = sm;
            services = se;
        }

    }
}