﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignment_1_b_n01330654
{
    public partial class assignment_1_b_n01330654 : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void HotelBook(object sender, EventArgs e)
        {


            //======CLIENTS
             string lname = clientLname.Text.ToString();
             string fname = clientFname.Text.ToString();
             string email = clientEmail.Text.ToString();
             string phone = clientPhone.Text.ToString();

             Clients newclient = new Clients();

             newclient.ClientLname = lname;
             newclient.ClientFname = fname;
             newclient.ClientPhone = phone;
             newclient.ClientEmail = email;


            //======HOTEL
            string roomType = hotelRoomType.SelectedItem.Text.ToString();// I did the SelectedItem.Text insted of SelectedItem.Value at some point I can't show the text value of the list.
            int numRoom = int.Parse(hotelNumRoom.Text);
            string smokingRoom = hotelSmokingRoom.SelectedItem.Text.ToString();
            List<string> services = new List<string>();

            Hotel newhotel = new Hotel(roomType, numRoom, smokingRoom, services);

            // SOURCE CODE == CHRISTINE BITTLE'S WEEK 4 SAMPLE CODE DURING THE CLASS FOR CHECKBOX LIST

            List<string> addservices = new List<string>();
            foreach (Control control in hotelAddServices.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox hotelService = (CheckBox)control;

                    if (hotelService.Checked)
                    {
                        addservices.Add(hotelService.Text);
                    }

                }
                 
            }
            newhotel.services = newhotel.services.Concat(addservices).ToList();

            //======BOOKING 
            string currentDate = DateTime.Today.ToString("dd-MM-yyyy");
            DateTime cdate = DateTime.ParseExact(currentDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            string bookingDate = cdate.ToString("dd/MM/yyyy");
            string checkin = bookingCheckin.Text.ToString();
            int nights = int.Parse(bookingNumNights.Text);

            Booking newbooking = new Booking(bookingDate, checkin, nights);

            //======BookOrder
            BookOrder newbookorder = new BookOrder(newhotel, newclient, newbooking);

            BookOrderRes.InnerHtml = newbookorder.BookOrderSummary();
        }

    }
 

  
}