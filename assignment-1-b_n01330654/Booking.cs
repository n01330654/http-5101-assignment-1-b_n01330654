﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignment_1_b_n01330654
{
    public class Booking
    {

        public string bookingDate;
        public string checkin;
        public int nights;
        
        public Booking(string t, string c, int n)
        {

            bookingDate = t;
            checkin = c;
            nights = n;
   
        }
    }
}