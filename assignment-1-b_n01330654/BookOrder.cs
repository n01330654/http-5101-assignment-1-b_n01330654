﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignment_1_b_n01330654
{
    public class BookOrder
    {
        public Clients clients;
        public Hotel hotel;
        public Booking booking;


        public BookOrder(Hotel h, Clients c, Booking b)
        {
            hotel = h;
            clients = c;
            booking = b;
        }

    public string BookOrderSummary()
        {
          
            string bookingSummary = "<======== HOTEL BOOKING SUMMARY AND RECEIPT ========><br><br>";

            bookingSummary += "<======== THANK YOU FOR BOOKING WITH US! ========>" + "<br>" + "<br>";
            bookingSummary += "Full Name: " + clients.ClientFname.ToString() + " " + clients.ClientLname.ToString() + "<br>";
            bookingSummary += "Phone Number: " + clients.ClientPhone.ToString() + "<br>";
            bookingSummary += "Email: " + clients.ClientEmail.ToString() + "<br>";

            bookingSummary += "<br>" + "<br>" + "<======== BOOKING SUMMARY ========>" + "<br>";
            bookingSummary += "Checkin Date: " + booking.checkin.ToString() + "<br>";
            bookingSummary += "Number of Nights: " + booking.nights.ToString() + "<br>";
            bookingSummary += "Number of Rooms: " + hotel.numRoom.ToString() + "<br>";
            bookingSummary += "Room Type: " + hotel.roomType.ToString() + "<br>";
            bookingSummary += "Room Other Option: " + hotel.smokingRoom.ToString() + "<br>";
            bookingSummary += "Additional Services: " + String.Join(",   ", hotel.services.ToArray()) + "<br>";

            bookingSummary += "<br>" + "<br>" + "<======== BOOKING TOTAL ========>" + "<br>";
            bookingSummary += "Booking Date: " + booking.bookingDate.ToString() + "<br>";
            bookingSummary += "Booking Total: " +  "$" + " " + BookingTotal().ToString() + " " +  "CAD" + "<br>" + "<br>";

            return bookingSummary;
        }

        public double BookingTotal()
        {
            double total = 0;

            if (hotel.roomType == "Twin Room")
            {
                total = booking.nights * hotel.numRoom * 29.99;
            }
            else if (hotel.roomType == "Deluxe Room")
            {
                total = booking.nights * hotel.numRoom * 39.99;
            }
            else if (hotel.roomType == "Family Room")
            {
                total = booking.nights * hotel.numRoom * 49.99;
            }

            if (total> 160)
            {
                total = total - (total * .20);
            }
            return total;
        }

    }
}